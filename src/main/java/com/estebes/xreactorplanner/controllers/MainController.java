package com.estebes.xreactorplanner.controllers;

import com.estebes.xreactorplanner.logic.Reactor;
import com.estebes.xreactorplanner.logic.ReactorComponentFactory;
import com.estebes.xreactorplanner.logic.Util;
import com.estebes.xreactorplanner.logic.components.DamageableReactorComponent;
import com.estebes.xreactorplanner.logic.components.ReactorComponent;
import com.estebes.xreactorplanner.widgets.InventorySlot;
import com.estebes.xreactorplanner.widgets.ReactorSlot;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;

/**
 * Created by estebes on 24-11-2016.
 */
public class MainController {

    @FXML
    private void initialize() {
        assert simulateButton != null;
        assert console != null;
        simulateButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                console.clear();
                //System.out.println(Reactor.instance.toString());
                Reactor.instance.simulate();
                String aux;
                while((aux = Reactor.instance.getConsoleOutput().poll()) != null) {
                    console.appendText(aux);
                    console.appendText(System.getProperty("line.separator"));
                }

                for (int row = 0; row < 6; row ++) {
                    for (int col = 0; col < 9; col++) {
                        ReactorSlot reactorSlot = (ReactorSlot) reactorGrid.getChildren().get(Util.coordsToIndex(row, col, 9));

                        int baseWidth = (int) reactorSlot.getWidth();
                        ReactorComponent comp = Reactor.instance.getComponent(row, col);
                        if(comp instanceof DamageableReactorComponent) {
                            DamageableReactorComponent dcomp = (DamageableReactorComponent) comp;
                            double mult = (double) dcomp.getDurability() / (double) dcomp.getMaxDurability();
                            reactorSlot.setDurability(baseWidth - 12, baseWidth - 15, (baseWidth - 15) - ((baseWidth - 15) * mult));
                        } else {
                            reactorSlot.setDurability(baseWidth - 12, baseWidth - 15, baseWidth - 15);
                        }
                    }
                }
            }
        });

        assert reactorGrid != null;

        for (int row = 0; row < 6; row ++) {
            for (int col = 0; col < 9; col++) {
                ReactorSlot reactorSlot = new ReactorSlot();
                reactorSlot.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

                final int x = row;
                final int y = col;
                reactorSlot.getButton().setOnMousePressed(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        if(event.isPrimaryButtonDown()) {
                            reactorSlot.setReactorComponent(selected.create());
                            Reactor.instance.addComponent(x, y, selected.create());
                        } else if (event.isSecondaryButtonDown()) {
                            reactorSlot.setReactorComponent(null);
                            Reactor.instance.removeComponent(x, y);
                        }
                        int aux = (int) reactorSlot.getWidth();
                        reactorSlot.setDurability(aux - 12, aux - 15, aux - 15);
                    }
                });

                // Add to the reactor grid
                reactorGrid.add(reactorSlot, col, row);
            }
        }


        assert inventoryGrid != null;

        for (int row = 0; row < 3; row ++) {
            for (int col = 0; col < 9; col++) {
                InventorySlot inventorySlot = new InventorySlot(ReactorComponentFactory.values()[Util.coordsToIndex(row, col, 9)]);
                inventorySlot.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

                inventorySlot.getButton().setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        if(event.isPrimaryButtonDown()) {
                            inventorySlot.getButton().setSelected(true);
                        }
                    }
                });

                inventorySlot.getButton().setOnMousePressed(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        if(event.isPrimaryButtonDown()) {
                            selected = inventorySlot.getReference();
                        }
                    }
                });

                // Add to the reactor grid
                inventorySlot.getButton().setToggleGroup(toggleGroup);
                inventoryGrid.add(inventorySlot, col, row);
            }
        }
        ((InventorySlot) inventoryGrid.getChildren().get(0)).getButton().setSelected(true);
        selected = ((InventorySlot) inventoryGrid.getChildren().get(0)).getReference();
    }

    @FXML private Button simulateButton;
    @FXML private GridPane reactorGrid;
    @FXML private GridPane inventoryGrid;
    @FXML private TextArea console;
    private final ToggleGroup toggleGroup = new ToggleGroup();
    private static ReactorComponentFactory selected;
}
