package com.estebes.xreactorplanner;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class XReactorPlanner extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void init() throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/xreactorplanner.fxml"));
        root = fxmlLoader.load();
    }

    @Override
    public void start(Stage stage) throws Exception {
        Scene scene = new Scene(root, 1280, 720);
        stage.setMinWidth(1280.0D);
        stage.setMinHeight(720.0D);
        stage.setScene(scene);
        stage.setTitle(Reference.TITLE + " - " + Reference.VERSION);
        stage.getIcons().add(new Image("/textures/xreactorplanner.png"));
        stage.show();
    }

    private AnchorPane root;
}
