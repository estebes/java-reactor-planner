package com.estebes.xreactorplanner.logic;

public class Util {
    public static int triangularNumber(int number) {
        return (int) ((number * number + number) / 2);
    }

    public static int coordsToIndex(int xCoord, int yCoord, int ySize) {
        return coordsToShifted(xCoord, yCoord, ySize, 0);
    }

    public static int coordsToShifted(int xCoord, int yCoord, int ySize, int shifted) {
        return (xCoord * ySize) + yCoord + shifted;
    }
}
