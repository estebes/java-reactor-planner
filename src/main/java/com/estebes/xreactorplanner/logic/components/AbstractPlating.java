package com.estebes.xreactorplanner.logic.components;

import com.estebes.xreactorplanner.logic.Reactor;

public class AbstractPlating extends ReactorComponent {
    public AbstractPlating(String name, String texture, int providedHeat, float explosionModifier) {
        super(name,  texture);
        this.providedHeat = providedHeat;
        this.explosionModifier = explosionModifier;
    }

    @Override
    public void processChamber(Reactor reactor, int xCoord, int yCoord) {
        reactor.addMaxHeat(providedHeat);
    }

    @Override
    public float influenceExplosion(Reactor reactor, int xCoord, int yCoord) {
        return explosionModifier;
    }

    /**
     * Vars
     */
    protected int providedHeat;
    protected float explosionModifier;
}
