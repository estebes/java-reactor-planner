package com.estebes.xreactorplanner.logic.components;

import com.estebes.xreactorplanner.logic.Reactor;

import java.util.ArrayDeque;

/**
 * Created by estebes on 28-11-2016.
 */
public class AbstractHeatSpreader extends ReactorComponent {
    public AbstractHeatSpreader(String name, String texture, int sideVent) {
        super(name, texture);
        this.sideVent = sideVent;
    }

    @Override
    public void processChamber(Reactor reactor , int xCoord, int yCoord) {
        ArrayDeque<Integer> heatAcceptors = new ArrayDeque<Integer>();

        if(reactor.checkHeatAcceptor(xCoord - 1, yCoord)) {
            heatAcceptors.add(xCoord - 1);
            heatAcceptors.add(yCoord);
        }
        if(reactor.checkHeatAcceptor(xCoord + 1, yCoord)) {
            heatAcceptors.add(xCoord + 1);
            heatAcceptors.add(yCoord);
        }
        if(reactor.checkHeatAcceptor(xCoord, yCoord - 1)) {
            heatAcceptors.add(xCoord);
            heatAcceptors.add(yCoord - 1);
        }
        if(reactor.checkHeatAcceptor(xCoord, yCoord + 1)) {
            heatAcceptors.add(xCoord);
            heatAcceptors.add(yCoord + 1);
        }

        while(!heatAcceptors.isEmpty()) {
            int selfHeat = reactor.alterHeatAt(heatAcceptors.pop(), heatAcceptors.pop(), -sideVent);

            if(selfHeat <= 0) {

            }
        }
    }

    private int sideVent;
}
