package com.estebes.xreactorplanner.logic.components;

import com.estebes.xreactorplanner.logic.Reactor;
import com.estebes.xreactorplanner.logic.Util;

import java.util.ArrayDeque;

public class AbstractMOXRod extends DamageableReactorComponent {
    public AbstractMOXRod(String name, String texture, int rods) {
        super(name, texture, 10000);
        this.rods = rods;
    }

    @Override
    public void processChamber(Reactor reactor , int xCoord, int yCoord) {
        int basePulses = 1 + Math.round(rods / 2);

        for(int r = 0; r < rods; r++) {
            int totalPulses = basePulses;

            for(int pulse = 0; pulse < totalPulses; pulse++) {
                this.pulse(reactor, xCoord, yCoord, xCoord, yCoord);
            }

            totalPulses += reactor.pulseAt(xCoord - 1, yCoord, xCoord, yCoord);
            totalPulses += reactor.pulseAt(xCoord + 1, yCoord, xCoord, yCoord);
            totalPulses += reactor.pulseAt(xCoord, yCoord - 1, xCoord, yCoord);
            totalPulses += reactor.pulseAt(xCoord, yCoord + 1, xCoord, yCoord);

            int heatGenerated = Util.triangularNumber(totalPulses) * 4;

            ArrayDeque<Integer> heatAcceptors = new ArrayDeque<Integer>();

            if(reactor.checkHeatAcceptor(xCoord - 1, yCoord)) {
                heatAcceptors.add(xCoord - 1);
                heatAcceptors.add(yCoord);
            }
            if(reactor.checkHeatAcceptor(xCoord + 1, yCoord)) {
                heatAcceptors.add(xCoord + 1);
                heatAcceptors.add(yCoord);
            }
            if(reactor.checkHeatAcceptor(xCoord, yCoord - 1)) {
                heatAcceptors.add(xCoord);
                heatAcceptors.add(yCoord - 1);
            }
            if(reactor.checkHeatAcceptor(xCoord, yCoord + 1)) {
                heatAcceptors.add(xCoord);
                heatAcceptors.add(yCoord + 1);
            }

            while(!heatAcceptors.isEmpty() && heatGenerated > 0) {
                int averageHeat = (int) (heatGenerated / (int) (heatAcceptors.size() / 2));
                heatGenerated -= averageHeat;
                averageHeat = reactor.alterHeatAt(heatAcceptors.pop(), heatAcceptors.pop(), averageHeat);
                heatGenerated += averageHeat;
            }

            if(heatGenerated > 0) {
                reactor.addHeat(heatGenerated);
            }
        }

        this.durability++;
        if(this.durability >= this.maxDurability - 1) {
            reactor.destroyComponent(xCoord, yCoord, this.name);
        }
    }

    @Override
    public boolean pulse(Reactor reactor, int xTarget, int yTarget, int xFrom, int yFrom) {
        float multiplier = reactor.getHeat() / reactor.getMaxHeat();
        reactor.addOutputEUTick((int ) (5 * (4 * multiplier + 1)));
        return true;
    }

    /**
     * Vars
     */
    private int rods;
}
