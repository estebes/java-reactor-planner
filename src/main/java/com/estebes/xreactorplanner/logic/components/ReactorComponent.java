package com.estebes.xreactorplanner.logic.components;

import com.estebes.xreactorplanner.logic.Reactor;

public abstract class ReactorComponent {
    public ReactorComponent(String name, String texture) {
        this.name = name;
        this.texture = texture;
        this.isDestroyed = false;
    }

    public void processChamber(Reactor reactor , int xCoord, int yCoord) {
        // NO-OP
    }

    public boolean pulse(Reactor reactor, int xTarget, int yTarget, int xFrom, int yFrom) {
        return false;
    }

    public boolean canStoreHeat(Reactor reactor , int xCoord, int yCoord) {
        return false;
    }

    public int getMaxHeat(Reactor reactor , int xCoord, int yCoord) {
        return 0;
    }

    public int getCurrentHeat(Reactor reactor , int xCoord, int yCoord) {
        return 0;
    }

    public int alterHeat(Reactor reactor , int xCoord, int yCoord, int heat) {
        return 0;
    }

    public float influenceExplosion(Reactor reactor , int xCoord, int yCoord) {
        return 0.0F;
    }

    public boolean isDestroyed() {
        return isDestroyed;
    }

    public void destroy() {
        isDestroyed = true;
    }

    public void reset() {
        isDestroyed = false;
    }

    /**
     * Getters
     */
    public String getName() {
        return name;
    }

    public String getTexture() {
        return texture;
    }

    /**
     * Vars
     */
    protected final String name;
    protected final String texture;
    protected Boolean isDestroyed;
}
