package com.estebes.xreactorplanner.logic.components;

import com.estebes.xreactorplanner.logic.Reactor;

public class AbstractDamageableReflector extends DamageableReactorComponent {
    public AbstractDamageableReflector(String name, String texture, int maxDurability) {
        super(name, texture, maxDurability);
    }

    @Override
    public boolean pulse(Reactor reactor, int xTarget, int yTarget, int xFrom, int yFrom) {
        reactor.pulseAt(xFrom, yFrom, xTarget, yTarget);

        this.durability++;
        if(this.durability >= this.maxDurability - 1) {
            reactor.destroyComponent(xTarget, yTarget, this.name);
        }

        return true;
    }

    @Override
    public float influenceExplosion(Reactor reactor, int xCoord, int yCoord) {
        return -1.0F;
    }
}
