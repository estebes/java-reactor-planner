package com.estebes.xreactorplanner.logic.components;

import com.estebes.xreactorplanner.logic.Reactor;

public class AbstractReflector extends ReactorComponent {
    public AbstractReflector(String name, String texture) {
        super(name, texture);
    }

    @Override
    public boolean pulse(Reactor reactor, int xTarget, int yTarget, int xFrom, int yFrom) {
        reactor.pulseAt(xFrom, yFrom, xTarget, yTarget);
        return true;
    }

    @Override
    public float influenceExplosion(Reactor reactor, int xCoord, int yCoord) {
        return -1.0F;
    }
}
