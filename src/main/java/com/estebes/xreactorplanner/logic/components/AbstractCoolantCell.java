package com.estebes.xreactorplanner.logic.components;

import com.estebes.xreactorplanner.logic.Reactor;

public class AbstractCoolantCell extends DamageableReactorComponent {
    public AbstractCoolantCell(String name, String texture, int maxDurability) {
        super(name, texture, maxDurability);
    }

    @Override
    public boolean canStoreHeat(Reactor reactor , int xCoord, int yCoord) {
        return true;
    }

    @Override
    public int getMaxHeat(Reactor reactor , int xCoord, int yCoord) {
        return this.maxDurability;
    }

    @Override
    public int getCurrentHeat(Reactor reactor , int xCoord, int yCoord) {
        return this.durability;
    }

    @Override
    public int alterHeat(Reactor reactor , int xCoord, int yCoord, int heat) {
        this.durability += heat;

        if(this.durability >= this.maxDurability - 1) {
            reactor.destroyComponent(xCoord, yCoord, this.name);
        } else {
            if(this.durability < 0) {
                heat = this.durability;
                this.durability = 0;
            } else {
                heat = 0;
            }
        }

        return heat;
    }
}
