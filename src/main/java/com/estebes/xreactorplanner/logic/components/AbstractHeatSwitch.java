package com.estebes.xreactorplanner.logic.components;

import com.estebes.xreactorplanner.logic.Reactor;

import java.util.ArrayDeque;

/**
 * Created by estebes on 28-11-2016.
 */
public class AbstractHeatSwitch extends DamageableReactorComponent {
    public AbstractHeatSwitch(String name, String texture, int maxDurability, int sideVent, int reactorVent) {
        super(name, texture, maxDurability);
        this.sideVent = sideVent;
        this.reactorVent = reactorVent;
    }

    @Override
    public void processChamber(Reactor reactor , int xCoord, int yCoord) {
        int myHeat = 0;
        ArrayDeque<Integer> heatAcceptors = new ArrayDeque<Integer>();

        if(sideVent > 0) {
            if(reactor.checkHeatAcceptor(xCoord - 1, yCoord)) {
                heatAcceptors.add(xCoord - 1);
                heatAcceptors.add(yCoord);
            }
            if(reactor.checkHeatAcceptor(xCoord + 1, yCoord)) {
                heatAcceptors.add(xCoord + 1);
                heatAcceptors.add(yCoord);
            }
            if(reactor.checkHeatAcceptor(xCoord, yCoord - 1)) {
                heatAcceptors.add(xCoord);
                heatAcceptors.add(yCoord - 1);
            }
            if(reactor.checkHeatAcceptor(xCoord, yCoord + 1)) {
                heatAcceptors.add(xCoord);
                heatAcceptors.add(yCoord + 1);
            }
        }

        if(sideVent > 0) {
            while(!heatAcceptors.isEmpty()) {
                int xTarget = heatAcceptors.pop();
                int yTarget = heatAcceptors.pop();
                DamageableReactorComponent heatable = (DamageableReactorComponent) reactor.getComponent(xTarget, yTarget);

                double myMed = (double) this.durability * 100 / this.maxDurability;
                double heatableMed = (double) heatable.getDurability() * 100 / heatable.getMaxDurability();

                int add = (int) (((double) heatable.getMaxDurability() / 100) * (heatableMed + myMed / 2));
                if(add > sideVent) {
                    add = sideVent;
                }

                if (heatableMed + myMed / 2 < 1) {
                    add = sideVent / 2;
                }
                if (heatableMed + myMed / 2 < 0.75) {
                    add = sideVent / 4;
                }
                if (heatableMed + myMed / 2 < 0.5) {
                    add = sideVent / 8;
                }
                if (heatableMed + myMed / 2 < 0.25) {
                    add = 1;
                }

                if ((double) Math.round(heatableMed * 10) / 10 > (double) Math.round(myMed * 10) / 10) {
                    add -= ( 2 * add);
                } else if ((double) Math.round(heatableMed * 10 ) / 10 == (double) Math.round(myMed * 10) / 10) {
                    add = 0;
                }

                myHeat -= add;
                add = heatable.alterHeat(reactor, xTarget, yTarget, add);
                myHeat += add;
            }
        }

        if(reactorVent > 0) {
            double myMed = (double) this.durability * 100 / this.maxDurability;
            double reactorMed = (double) reactor.getHeat() * 100 / reactor.getMaxHeat();

            int add = (int) (((double) reactor.getMaxHeat() / 100) * (reactorMed + myMed / 2));
            if(add > reactorVent) {
                add = reactorVent;
            }

            if (reactorMed + myMed / 2 < 1) {
                add = sideVent / 2;
            }
            if (reactorMed + myMed / 2 < 0.75) {
                add = sideVent / 4;
            }
            if (reactorMed + myMed / 2 < 0.5) {
                add = sideVent / 8;
            }
            if (reactorMed + myMed / 2 < 0.25) {
                add = 1;
            }

            if ((double) Math.round(reactorMed * 10) / 10 > (double) Math.round(myMed * 10) / 10) {
                add -= ( 2 * add);
            } else if ((double) Math.round(reactorMed * 10 ) / 10 == (double) Math.round(myMed * 10) / 10) {
                add = 0;
            }

            myHeat -= add;
            reactor.setHeat(reactor.getHeat() + add);
        }

        this.alterHeat(reactor, xCoord, yCoord, myHeat);
    }

    @Override
    public boolean canStoreHeat(Reactor reactor , int xCoord, int yCoord) {
        return true;
    }

    @Override
    public int getMaxHeat(Reactor reactor , int xCoord, int yCoord) {
        return this.maxDurability;
    }

    @Override
    public int getCurrentHeat(Reactor reactor , int xCoord, int yCoord) {
        return this.durability;
    }

    @Override
    public int alterHeat(Reactor reactor , int xCoord, int yCoord, int heat) {
        durability += heat;
        if(this.durability >= this.maxDurability) {
            reactor.destroyComponent(xCoord, yCoord, this.name);
        } else {
            if(this.durability < 0) {
                heat = this.durability;
                this.durability = 0;
            } else {
                heat = 0;
            }
        }
        return heat;
    }

    private int sideVent;
    private int reactorVent;
}
