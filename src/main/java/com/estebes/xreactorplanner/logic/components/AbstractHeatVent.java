package com.estebes.xreactorplanner.logic.components;

import com.estebes.xreactorplanner.logic.Reactor;

/**
 * Created by estebes on 27-11-2016.
 */
public class AbstractHeatVent extends DamageableReactorComponent {
    public AbstractHeatVent(String name, String texture, int maxDurability, int sideVent, int reactorVent) {
        super(name, texture, maxDurability);
        this.sideVent = sideVent;
        this.reactorVent = reactorVent;
    }

    @Override
    public void processChamber(Reactor reactor , int xCoord, int yCoord) {
        if(reactorVent > 0) {
            int reactorHeat = reactor.getHeat();
            int reactorHeatDrain = reactorHeat;

            reactorHeatDrain = (reactorHeatDrain > reactorVent) ? reactorVent : reactorHeatDrain;
            reactorHeat -= reactorHeatDrain;

            if(reactor.alterHeatAt(xCoord, yCoord, reactorHeatDrain) > 0) {
                return;
            }

            reactor.setHeat(reactorHeat);
        }

        int selfHeat = reactor.alterHeatAt(xCoord, yCoord, -sideVent);

        if(selfHeat  <= 0) {

        }
    }

    @Override
    public boolean canStoreHeat(Reactor reactor , int xCoord, int yCoord) {
        return true;
    }

    @Override
    public int getMaxHeat(Reactor reactor , int xCoord, int yCoord) {
        return this.maxDurability;
    }

    @Override
    public int getCurrentHeat(Reactor reactor , int xCoord, int yCoord) {
        return this.durability;
    }

    @Override
    public int alterHeat(Reactor reactor , int xCoord, int yCoord, int heat) {
        durability += heat;
        if(this.durability >= this.maxDurability) {
            reactor.destroyComponent(xCoord, yCoord, this.name);
        } else {
            if(this.durability < 0) {
                heat = this.durability;
                this.durability = 0;
            } else {
                heat = 0;
            }
        }
        return heat;
    }

    private int sideVent;
    private int reactorVent;
}
