package com.estebes.xreactorplanner.logic.components;

import com.estebes.xreactorplanner.logic.Reactor;

public class AbstractCondensator extends DamageableReactorComponent {
    public AbstractCondensator(String name, String texture, int maxDurability) {
        super(name, texture, maxDurability);
    }

    @Override
    public boolean canStoreHeat(Reactor reactor , int xCoord, int yCoord) {
        return this.durability < this.maxDurability;
    }

    @Override
    public int getMaxHeat(Reactor reactor , int xCoord, int yCoord) {
        return this.maxDurability;
    }

    @Override
    public int getCurrentHeat(Reactor reactor , int xCoord, int yCoord) {
        return this.durability;
    }

    @Override
    public int alterHeat(Reactor reactor , int xCoord, int yCoord, int heat) {
        if(heat < 0) {
            return heat;
        }

        int  aux = Math.min(heat, this.maxDurability - this.durability);
        heat -= aux;
        this.durability += aux;

        return heat;
    }
}
