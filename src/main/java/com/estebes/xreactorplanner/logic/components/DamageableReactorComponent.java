package com.estebes.xreactorplanner.logic.components;

public abstract class DamageableReactorComponent extends ReactorComponent {
    public DamageableReactorComponent(String name, String texture, int maxDurability) {
        super(name, texture);
        this.durability = 0;
        this.maxDurability = maxDurability;
    }

    /**
     * Getters and Setters
     */
    public int getDurability() {
        return durability;
    }

    public void setDurability(int durability) {
        this.durability = durability;
    }

    public int getMaxDurability() {
        return maxDurability;
    }

    public void setMaxDurability(int maxDurability) {
        this.maxDurability = maxDurability;
    }

    @Override
    public void reset() {
        super.reset();
        this.durability = 0;
    }

    /**
     * Vars
     */
    protected int durability, maxDurability;
}

