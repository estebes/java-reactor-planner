package com.estebes.xreactorplanner.logic;

import com.estebes.xreactorplanner.logic.components.AbstractCondensator;
import com.estebes.xreactorplanner.logic.components.AbstractCoolantCell;
import com.estebes.xreactorplanner.logic.components.AbstractDamageableReflector;
import com.estebes.xreactorplanner.logic.components.AbstractHeatSpreader;
import com.estebes.xreactorplanner.logic.components.AbstractHeatSwitch;
import com.estebes.xreactorplanner.logic.components.AbstractHeatVent;
import com.estebes.xreactorplanner.logic.components.AbstractMOXRod;
import com.estebes.xreactorplanner.logic.components.AbstractPlating;
import com.estebes.xreactorplanner.logic.components.AbstractReflector;
import com.estebes.xreactorplanner.logic.components.AbstractUraniumRod;
import com.estebes.xreactorplanner.logic.components.ReactorComponent;

public enum ReactorComponentFactory {
    NONE {
        /**
         * @return null
         */
        @Override
        public ReactorComponent create() {
            return null;
        }
    },
    SINGLE_URANIUM_ROD {
        /**
         * @return Fuel Rod (Uranium)
         */
        @Override
        public ReactorComponent create() {
            return new AbstractUraniumRod("Fuel Rod (Uranium)", "single_uranium_rod", 1);
        }
    },
    DUAL_URANIUM_ROD {
        /**
         * @return Dual Fuel Rod (Uranium)
         */
        @Override
        public ReactorComponent create() {
            return new AbstractUraniumRod("Dual Fuel Rod (Uranium)", "dual_uranium_rod", 2);
        }
    },
    QUAD_URANIUM_ROD {
        /**
         * @return Quad Fuel Rod (Uranium)
         */
        @Override
        public ReactorComponent create() {
            return new AbstractUraniumRod("Quad Fuel Rod (Uranium)", "quad_uranium_rod", 4);
        }
    },
    SINGLE_MOX_ROD {
        /**
         * @return Fuel Rod (MOX)
         */
        @Override
        public ReactorComponent create() {
            return new AbstractMOXRod("Fuel Rod (MOX)", "single_mox_rod", 1);
        }
    },
    DUAL_MOX_ROD {
        /**
         * @return Dual Fuel Rod (MOX)
         */
        @Override
        public ReactorComponent create() {
            return new AbstractMOXRod("Dual Fuel Rod (MOX)", "dual_mox_rod", 2);
        }
    },
    QUAD_MOX_ROD {
        /**
         * @return Quad Fuel Rod (MOX)
         */
        @Override
        public ReactorComponent create() {
            return new AbstractMOXRod("Quad Fuel Rod (MOX)", "quad_mox_rod", 4);
        }
    },
    REACTOR_PLATING {
        /**
         * @return Reactor Plating
         */
        @Override
        public ReactorComponent create() {
            return new AbstractPlating("Reactor Plating", "reactor_plating", 1000, 0.95F);
        }
    },
    HEAT_CAPACITY_REACTOR_PLATING {
        /**
         * @return Heat-Capacity Reactor Plating
         */
        @Override
        public ReactorComponent create() {
            return new AbstractPlating("Heat-Capacity Reactor Plating", "heat_capacity_reactor_plating", 2000, 0.99F);
        }
    },
    CONTAINMENT_REACTOR_PLATING {
        /**
         * @return Containment Reactor Plating
         */
        @Override
        public ReactorComponent create() {
            return new AbstractPlating("Containment Reactor Plating", "containment_reactor_plating", 500, 0.9F);
        }
    },
    RSH_CONDENSATOR {
        /**
         * @return RSH Condensator
         */
        @Override
        public ReactorComponent create() {
            return new AbstractCondensator("RSH Condensator", "rsh_condensator", 20000);
        }
    },
    LZH_CONDENSATOR {
        /**
         * @return LZH Condensator
         */
        @Override
        public ReactorComponent create() {
            return new AbstractCondensator("LZH Condensator", "lzh_condensator", 100000);
        }
    },
    HEAT_VENT {
        /**
         * @return Heat Vent
         */
        @Override
        public ReactorComponent create() {
            return new AbstractHeatVent("Heat Vent", "heat_vent", 1000, 6, 0);
        }
    },
    REACTOR_HEAT_VENT {
        /**
         * @return Reactor Heat Vent
         */
        @Override
        public ReactorComponent create() {
            return new AbstractHeatVent("Reactor Heat Vent", "reactor_heat_vent", 1000, 5, 5);
        }
    },
    OVERCLOCKED_HEAT_VENT {
        /**
         * @return Overclocked Heat Vent
         */
        @Override
        public ReactorComponent create() {
            return new AbstractHeatVent("Overclocked Heat Vent", "overclocked_heat_vent", 1000, 20, 36);
        }
    },
    ADVANCED_HEAT_VENT {
        /**
         * @return Advanced Heat Vent
         */
        @Override
        public ReactorComponent create() {
            return new AbstractHeatVent("Advanced Heat Vent", "advanced_heat_vent", 1000, 12, 0);
        }
    },
    COMPONENT_HEAT_VENT {
        /**
         * @return Component Heat Vent
         */
        @Override
        public ReactorComponent create() {
            return new AbstractHeatSpreader("Component Heat Vent", "component_heat_vent", 4);
        }
    },
    HEAT_EXCHANGER {
        /**
         * @return Heat Exchanger
         */
        @Override
        public ReactorComponent create() {
            return new AbstractHeatSwitch("Heat Exchanger", "heat_exchanger", 2500, 12, 4);
        }
    },
    REACTOR_HEAT_EXCHANGER {
        /**
         * @return Reactor Heat Exchanger
         */
        @Override
        public ReactorComponent create() {
            return new AbstractHeatSwitch("Reactor Heat Exchanger", "reactor_heat_exchanger", 5000, 0, 72);
        }
    },
    COMPONENT_HEAT_EXCHANGER {
        /**
         * @return Component Heat Exchanger
         */
        @Override
        public ReactorComponent create() {
            return new AbstractHeatSwitch("Component Heat Exchanger", "component_heat_exchanger", 5000, 36, 0);
        }
    },
    ADVANCED_HEAT_EXCHANGER {
        /**
         * @return Advanced Heat Exchanger
         */
        @Override
        public ReactorComponent create() {
            return new AbstractHeatSwitch("Advanced Heat Exchanger", "advanced_heat_exchanger", 10000, 24, 8);
        }
    },
    NEUTRON_REFLECTOR {
        /**
         * @return Neutron Reflector
         */
        @Override
        public ReactorComponent create() {
            return new AbstractDamageableReflector("Neutron Reflector", "neutron_reflector", 30000);
        }
    },
    THICK_NEUTRON_REFLECTOR {
        /**
         * @return Thick Neutron Reflector
         */
        @Override
        public ReactorComponent create() {
            return new AbstractDamageableReflector("Thick Neutron Reflector", "thick_neutron_reflector", 120000);
        }
    },
    IRIDIUM_NEUTRON_REFLECTOR {
        /**
         * @return Iridium Neutron Reflector
         */
        @Override
        public ReactorComponent create() {
            return new AbstractReflector("Iridium Neutron Reflector", "iridium_neutron_reflector");
        }
    },
    COOLANT_CELL_10K {
        /**
         * @return 10K Coolant Cell
         */
        @Override
        public ReactorComponent create() {
            return new AbstractCoolantCell("10K Coolant Cell", "coolant_cell_10k", 10000);
        }
    },
    COOLANT_CELL_30K {
        /**
         * @return 30K Coolant Cell
         */
        @Override
        public ReactorComponent create() {
            return new AbstractCoolantCell("30K Coolant Cell", "coolant_cell_30k", 30000);
        }
    },
    COOLANT_CELL_60K {
        /**
         * @return 60K Coolant Cell
         */
        @Override
        public ReactorComponent create() {
            return new AbstractCoolantCell("60K Coolant Cell", "coolant_cell_60k", 60000);
        }
    };

    public abstract ReactorComponent create();
}
