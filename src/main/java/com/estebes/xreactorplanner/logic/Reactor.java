package com.estebes.xreactorplanner.logic;

import com.estebes.xreactorplanner.logic.components.ReactorComponent;

import java.util.ArrayDeque;
import java.util.Locale;
import java.util.Queue;

public class Reactor {
    /**
     * Reactor instance. Any reactor references will use this.
     */
    public static final Reactor instance = new Reactor();

    public Reactor() {
        reactorGrid = new ReactorComponent[rXSize][rYSize];
        for(int x = 0; x < rXSize; x++) {
            for(int y = 0; y < rYSize; y++) {
                reactorGrid[x][y] = null;
            }
        }

        // Reactor properties
        heat = 0;
        maxHeat = 10000;

        // Time counters
        cycleTicks = 0;

        // Energy counters
        outputEUTick = 0;
        outputEUCycle = 0;
        outputEUMin = 0;
        outputEUMax = 0;
    }

    // Component handling
    public void addComponent(int xCoord, int yCoord, ReactorComponent reactorComponent) {
        reactorGrid[xCoord][yCoord] = reactorComponent;
    }

    public ReactorComponent getComponent(int xCoord, int yCoord) {
        return reactorGrid[xCoord][yCoord];
    }

    public void removeComponent(int xCoord, int yCoord) {
        reactorGrid[xCoord][yCoord] = null;
    }

    public void destroyComponent(int xCoord, int yCoord, String name) {
        reactorGrid[xCoord][yCoord].destroy();
        String ret = String.format(Locale.getDefault(), "%s at coords (%d,%d) was destroyed after %d seconds",
                name, xCoord, yCoord, cycleTicks / 20);
        consoleOutput.add(ret);
    }

    //
    public boolean checkHeatAcceptor(int xTarget, int yTarget) {
        if(!validCoords(xTarget, yTarget)) {
            return false;
        }

        if(reactorGrid[xTarget][yTarget] == null) {
            return false;
        }

        if(reactorGrid[xTarget][yTarget].isDestroyed()) {
            return false;
        }

        return reactorGrid[xTarget][yTarget].canStoreHeat(this, xTarget, yTarget);
    }

    //
    public int alterHeatAt(int xCoord, int yCoord, int heat) {
        if(!validCoords(xCoord, yCoord)) {
            return heat;
        }

        if(reactorGrid[xCoord][yCoord] == null) {
            return heat;
        }

        if(reactorGrid[xCoord][yCoord].isDestroyed()) {
            return heat;
        }

        return reactorGrid[xCoord][yCoord].alterHeat(this, xCoord, yCoord, heat);
    }

    //
    public int pulseAt(int xTarget, int yTarget, int xFrom, int yFrom) {
        if(!validCoords(xTarget, yTarget)) {
            return 0;
        }

        if(reactorGrid[xTarget][yTarget] == null) {
            return 0;
        }

        if(reactorGrid[xTarget][yTarget].isDestroyed()) {
            return 0;
        }

        return reactorGrid[xTarget][yTarget].pulse(this, xTarget, yTarget, xFrom, yFrom) ? 1 : 0;
    }

    public void simulate() {
        // Reset component durability
        for(int x = 0; x < rXSize; x++) {
            for(int y = 0; y < rYSize; y++) {
                if(reactorGrid[x][y] != null) {
                    reactorGrid[x][y].reset();
                }
            }
        }

        // Reset values
        heat = 0;
        maxHeat = 10000;
        cycleTicks = 0;
        outputEUTick = 0;
        outputEUCycle = 0;
        outputEUMin = 0;
        outputEUMax = 0;

        // Tick counter
        int tick = 0;

        // Calculate the values to use for the next 20 ticks
        do {
            if(tick == 0) {
                maxHeat = 10000;
                outputEUTick = 0;

                // Simulate
                for(int x = 0; x < rXSize; x++) {
                    for(int y = 0; y < rYSize; y++) {
                        if(reactorGrid[x][y] != null && !reactorGrid[x][y].isDestroyed()) {
                            reactorGrid[x][y].processChamber(this, x, y);
                        }
                    }
                }

                // "It will be a smoking crater!" aka reactor explosion
                if(heat > maxHeat) {
                    consoleOutput.add("It will be a smoking crater! - The reactor has exploded!!!");
                    break;
                }

                // Min EU/t
                if(outputEUTick != 0) {
                    outputEUMin = Math.min(outputEUMin, outputEUTick);
                    if(outputEUMin == 0) {
                        outputEUMin = outputEUTick;
                    }
                }

                // Max EU/t
                outputEUMax = Math.max(outputEUMax, outputEUTick);
            }

            if(outputEUTick != 0) {
                cycleTicks++;
                outputEUCycle += outputEUTick;
            }

            // 20 ticks have passed (In MC 20 ticks = 1 second)
            tick++;
            if(tick == 20) {
                tick = 0;
            }

        } while(outputEUTick > 0);

        // Add the simulation results to the deque
        consoleOutput.add(String.format(Locale.getDefault(), "Cycle duration (seconds): %d", (int) cycleTicks / 20));
        consoleOutput.add(String.format(Locale.getDefault(), "Cycle duration (ticks): %d", cycleTicks));
        consoleOutput.add(String.format(Locale.getDefault(), "Total Output: %3f", outputEUCycle));
        consoleOutput.add(String.format(Locale.getDefault(), "Reactor Heat: %d", heat));
        consoleOutput.add(String.format(Locale.getDefault(), "Average Eu/T: %3f",
                (cycleTicks == 0) ? 0.0F : outputEUCycle / cycleTicks));
        consoleOutput.add(String.format(Locale.getDefault(), "Min Eu/T: %3f", outputEUMin));
        consoleOutput.add(String.format(Locale.getDefault(), "Max Eu/T: %3f", outputEUMax));
    }

    public void printSimulationResults() {
        String aux;
        while((aux = consoleOutput.poll()) != null) {
            System.out.println(aux);
        }
    }

    public boolean validCoords(int x, int y) {
        return ((x >= 0 && x < rXSize) && (y >= 0 && y < rYSize));
    }

    public String getCode() {
        return null;
    }

    public void setCode() {

    }

    @Override
    public String toString() {
        StringBuilder ret = new StringBuilder();
        String delimiter = "";
        for(int x = 0; x < rXSize; x++) {
            for(int y = 0; y < rYSize; y++) {
                if(reactorGrid[x][y] != null) {
                    ret.append(delimiter).append(reactorGrid[x][y].getName());
                } else {
                    ret.append(delimiter).append("null");
                }
                delimiter = " ";
            }
            delimiter = System.getProperty("line.separator");
        }
        return ret.toString();
    }

    /**
     * Getters, Setters and Adders
     */
    public ReactorComponent[][] getReactorGrid() {
        return reactorGrid;
    }

    // Heat
    public int getHeat() {
        return heat;
    }

    public void setHeat(int heat) {
        this.heat = heat;
    }

    public void addHeat(int value) {
        heat += value;
    }

    // Max Heat
    public int getMaxHeat() {
        return maxHeat;
    }

    public void setMaxHeat(int maxHeat) {
        this.maxHeat = maxHeat;
    }

    public void addMaxHeat(int value) {
        maxHeat += value;
    }

    // Cycle Ticks
    public int getCycleTicks() {
        return cycleTicks;
    }

    // Output EU Tick
    public float getOutputEUTick() {
        return outputEUTick;
    }

    public void setOutputEUTick(int outputEUTick) {
        this.outputEUTick = outputEUTick;
    }

    public void addOutputEUTick(int value) {
        outputEUTick += value;
    }

    // Output EU Cycle
    public float getOutputEUCycle() {
        return outputEUCycle;
    }

    public float getOutputEUMin() {
        return outputEUMin;
    }

    public float getOutputEUMax() {
        return outputEUMax;
    }

    // Console Output
    public Queue<String> getConsoleOutput() {
        return consoleOutput;
    }

    /**
     * Vars
     */
    // Reactor grid
    private int rXSize = 6, rYSize = 9;
    private ReactorComponent[][] reactorGrid;

    // Reactor properties
    private int heat, maxHeat;

    // Time counters
    private int cycleTicks;

    // Power counters
    private float outputEUTick, outputEUCycle, outputEUMin, outputEUMax;

    // Heat counters
    private float heatTick, heatCycle, heatMin, heatMax;

    // Deque with the simulation data
    private Queue<String> consoleOutput = new ArrayDeque<String>();
}
