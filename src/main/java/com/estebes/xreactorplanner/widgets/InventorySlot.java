package com.estebes.xreactorplanner.widgets;

import com.estebes.xreactorplanner.logic.ReactorComponentFactory;
import com.estebes.xreactorplanner.logic.components.ReactorComponent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;

import java.io.IOException;

/**
 * Created by estebes on 25-11-2016.
 */
public class InventorySlot extends StackPane {
    public InventorySlot(ReactorComponentFactory reference) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/inventory_slot.fxml"));
        fxmlLoader.setController(this);
        try {
            Node node = (Node) fxmlLoader.load();
            this.getChildren().add(node);
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        this.reference = reference;
        try {
            ReactorComponent aux = reference.create();
            icon.setImage(new Image("/textures/icons/" + aux.getTexture() + ".png", 256, 256, false, false));
            button.setTooltip(new Tooltip(aux.getName()));
        } catch (NullPointerException exception) {
            icon.setImage(null);
            button.setTooltip(null);
        }
    }

    @FXML
    private void initialize() {
        assert button != null : "fx:id=button initialization failed.";
        assert icon != null : "fx:id=icon initialization failed.";
    }

    public ToggleButton getButton() {
        return this.button;
    }

    public ReactorComponentFactory getReference() {
        return reference;
    }

    @FXML private ToggleButton button;
    @FXML private ImageView icon;
    private final ReactorComponentFactory reference;
}
