package com.estebes.xreactorplanner.widgets;

import com.estebes.xreactorplanner.logic.components.DamageableReactorComponent;
import com.estebes.xreactorplanner.logic.components.ReactorComponent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Rectangle;

import java.io.IOException;

/**
 * Created by estebes on 25-11-2016.
 */
public class ReactorSlot extends StackPane {
    public ReactorSlot() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/reactor_slot.fxml"));
        fxmlLoader.setController(this);
        try {
            Node node = (Node) fxmlLoader.load();
            this.getChildren().add(node);
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
        this.reactorComponent = null;
    }

    @FXML
    private void initialize() {
        assert button != null : "fx:id=button initialization failed.";
        assert icon != null : "fx:id=icon initialization failed.";
        assert durabilityBarBG != null : "fx:id=durabilityBarBG initialization failed.";
        assert durabilityBarFG != null : "fx:id=durabilityBarFG initialization failed.";
        assert durabilityBar != null : "fx:id=durabilityBar initialization failed.";
    }

    public Button getButton() {
        return this.button;
    }

    public ReactorComponent getReactorComponent() {
        return reactorComponent;
    }

    public void setReactorComponent(ReactorComponent reactorComponent) {
        this.reactorComponent = reactorComponent;

        if(reactorComponent == null) {
            durabilityBarBG.setVisible(false);
            durabilityBarFG.setVisible(false);
            durabilityBar.setVisible(false);
        } else if(reactorComponent instanceof DamageableReactorComponent) {
            durabilityBarBG.setVisible(true);
            durabilityBarFG.setVisible(true);
            durabilityBar.setVisible(true);
        } else {
            durabilityBarBG.setVisible(false);
            durabilityBarFG.setVisible(false);
            durabilityBar.setVisible(false);
        }

        if(reactorComponent != null) {
            icon.setImage(new Image("/textures/icons/" + reactorComponent.getTexture() + ".png", this.getWidth(), this.getHeight(), false, false));
        } else {
            icon.setImage(null);
        }
    }

    public void setDurability(double a, double b, double c) {
        durabilityBarBG.setWidth(a);
        durabilityBarFG.setWidth(b);
        durabilityBar.setWidth(c);
    }

    @FXML private Button button;
    @FXML private ImageView icon;
    @FXML private Rectangle durabilityBarBG;
    @FXML private Rectangle durabilityBarFG;
    @FXML private Rectangle durabilityBar;
    private ReactorComponent reactorComponent;
}
