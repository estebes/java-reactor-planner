package com.estebes.xreactorplanner;

/**
 * Created by estebes on 16-11-2016.
 */
public class Reference {
    public static final String TITLE = "XReactorPlanner";
    public static final String VERSION = "0.0.1";
    public static final int WIDTH = 1280;
    public static final int HEIGHT = 720;
}
